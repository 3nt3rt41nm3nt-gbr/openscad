include <rails.scad>;
include <ladder.scad>;
include <rrx.scad>;
$fn = 80;

// station
rB = "RoyalBlue";  // RoyalBlue
g  = "Gray";        // Gray
sG = "SlateGray";  // SlateGray

module holder(height) {
	color(g) {
		translate([0, 0, 0]) {
			cube([300, 300, height]);
		}
	}
}

module holder_row(num_total, total_length, height) {
	for (i = [0:total_length / num_total:total_length]) {
		translate([ 0, i, 0 ]) {
			holder(height);
		}
	}
}

module rain_shelter(length, roof_rotation) {
	color(rb) {
		translate([ 0, length/2, 0]) {
			rotate([ 180, roof_rotation, 0 ]) {
				cube([ 200, length, 3000 ]);
			}
		}
	}
}

module rain_protection(length) {
	translate([5500 - 300 - 100, -length / 3 , 0]) {
		holder_row(15, length / 3 * 2, 3000);
	}
	translate([5500, 0, 3000]) rain_shelter(length / 3 * 2 + 8000, 100);
}

module track_pair(length) {
	height = 2000;
	track_canal_height = 1000;

	difference() {
		color(sG) {
			translate([ 0, 0, -height / 2]) {
				cube([ 11000, length, height ], center = true);
			}
		}

		// translate in order to prevent clipping
		translate([0, -1, 1]) {
			union() {
				translate([0, 0, -track_canal_height / 2]) {
					cube([5000, length + 1000, track_canal_height], center = true);
				}
				translate([0, 0, -track_canal_height / 2 - 100]) {
					cube([6500, length + 1000, track_canal_height - 100], center = true);
				}
			}
		}
	}

	rain_protection(length);
	mirror([1, 0, 0]) rain_protection(length);

	// rails
	for (i = [0:30000:30000*6]) {
		translate([0, -length/2 + i - 7500 , -track_canal_height - 50]) rail_30000mm();
		translate([-2500, -length/2 + i - 7500 , -track_canal_height - 50]) rail_30000mm();
	}
}
//rain_protection(200000);

for (i = [0:11000:11000*4]) {
	translate([i, 0, 0]) track_pair(200000);
}
