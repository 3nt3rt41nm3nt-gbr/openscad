$fn = 300;
door_elevation = 150;
door_width = 1500;
door_heigth = 2000;
depth = 20;

// CARRIAGE
// there are 3 different types of carriages. one of them is just mirrored.
// types:
//   * first carriage (1) - is round on the front end;.has one only floor;
//   * generic carriage (0) - is not rounded; has two floors;
//   * last carriage (-1) - is round on the back; has only one floor; the same
//   as the first carriage but mirrored on the z axis
module defaultCarriage(width, length, height) {
	color("Snow") {
		union() {
			difference() {
				union() {
					// height / 2 is used to compensate the cylinder on the top
					translate([ 0, 0, 750 ]) { cube([ length, width, height ]); }

					// rounded roof
					translate([ 0, width / 2, height + 750 ]) {
						rotate([ 0, 90, 0 ]) { cylinder(h = length, r = width / 2); }
					}

					// under part
					translate([ 0, 2820, 0 ]) {
						rotate([ 90, 0, 0 ]) {
							linear_extrude(2820) {
								polygon(points = [
										[ 0, 250 ], [ 500, 250 ], [ 750, 500 ], [ 3000, 500 ],
										[ 3500, 0 ],

										[ 26226 - 3500, 0 ], [ 26226 - 3000, 500 ],
										[ 26226 - 750, 500 ], [ 26226 - 500, 250 ],
										[ 26226 - 0, 250 ],

										[ 26226, 750 ], [ 0, 750 ]
								]);
							}
						}
					}
				}				}}
	}

	// WHEELS
	// below
	translate([ 1000, 500, 0 ]) { wheel(90); }
	translate([ 2800, 500, 0 ]) { wheel(90); }
	translate([ 1000, 2300, 0 ]) { wheel(-90); }
	translate([ 2800, 2300, 0 ]) { wheel(-90); }

	// above
	translate([ 23500, 500, 0 ]) { wheel(90); }
	translate([ 25200, 500, 0 ]) { wheel(90); }
	translate([ 23500, 2300, 0 ]) { wheel(-90); }
	translate([ 25200, 2300, 0 ]) { wheel(-90); }
}


module carriage(width, length, height) {
	defaultCarriage(2820, 26226,2000);
	union() {
		depth = 120;

		windowss = [
			// upper windows
			[ 7000, 50, 2600, 100, -10 ],
			[ 10000, 50, 2600, 100, -10 ],
			[ 13000, 50, 2600, 100, -10 ],
			[ 16000, 50, 2600, 100, -10 ],

			// upper windows
			[ 7000, width + 50, 2600, 100, 10 ],
			[ 10000, width + 50, 2600, 100, 10 ],
			[ 13000, width + 50, 2600, 100, 10 ],
			[ 16000, width + 50, 2600, 100, 10 ],

		];

			for (window_data = windowss) {
				position = [for (i = [0:2]) window_data[i]];
				translate(position) {
					rotation = window_data[4];
					window(2000, 800, 100, depth, rotation);
				}
			}
	}

	windows = [
		// bottom windows
		[ 7000, 0, 600, 100, 0 ],
		[ 10000, 0, 600, 100, 0 ],
		[ 13000, 0, 600, 100, 0 ],
		[ 16000, 0, 600, 100, 0 ],

		// bottom windows
		[ 7000, 2870, 600, 100, 0 ],
		[ 10000, 2870, 600, 100, 0 ],
		[ 13000, 2870, 600, 100, 0 ],
		[ 16000, 2870, 600, 100, 0 ],

		// upper windows
		[ 7000, 50, 2600, 100, -10 ],
		[ 10000, 50, 2600, 100, -10 ],
		[ 13000, 50, 2600, 100, -10 ],
		[ 16000, 50, 2600, 100, -10 ],

		// upper windows
		[ 7000, width - 50 + depth, 2600, 100, 10 ],
		[ 10000, width - 50 + depth, 2600, 100, 10 ],
		[ 13000, width - 50 + depth, 2600, 100, 10 ],
		[ 16000, width - 50 + depth, 2600, 100, 10 ],
			];

		for (window_data = windows) {
			position = [for (i = [0:2]) window_data[i]];
			translate(position) {
				type = window_data[3];
				rotation = window_data[4];
				window(2000, 800, depth, 100, rotation);
			}
		}


		l_windows = [

			[2000, 0, 1700, -10, 0],
			[2000, 2900, 1700, -10, 0],

			[23000, 0, 1700, -10, 1],
			[23000, 2900, 1700, -10, 1],

			[10250, 100, 1600, 100, -10, 2],
			[10250, 2970, 1600, 100, -10, 2],


			[ 4900, 100, 3200, 100, -10, 3],
			[ 19200, 100, 3200, 100, -10, 3],
			[ 4900, 2980, 3200, 100, -10, 3],
			[ 19200, 2980, 3200, 100, -10,3],
		];


			for (window_data = l_windows) {
				position = [for (i = [0:2]) window_data[i]];
				translate(position) {
					type = window_data[5];
					if (type == 0) {
						window(1200, 600, 20, 100);
					}
					else if (type == 1) {
						window(2100, 600, 20, 100);
					}
					else if (type == 2) {
						window(1500, 350, 222, 10);
					}
					else if (type == 3) {
						window(1800, 350, 222, 10);
					}			} }

			// DOORS
			// doors left
			translate([ 5000, -5, door_elevation ]) { door(door_heigth, door_width, 1); }

			translate([ length - 5000 - 2000, -5, door_elevation ]) {
				door(door_heigth, door_width, 1);
			}

			// doors right
			translate([ 5000, 2821 + 40, door_elevation ]) {
				door(door_heigth, door_width, 0);
			}

			translate([ length - 5000 - 2000, 2821 + 40, door_elevation ]) {
				door(door_heigth, door_width, 0);
			}
}

module wheel(rotation) {
	rotate([ rotation, 0, 0 ]) {
		cylinder(h = 100, r = 500);
		cylinder(h = 400, r = 450);
	}
}

module door(height, width, orientation) {
	foo = orientation == 1 ? -300 : 0;

	// door step thingy
	translate([ 0, foo, 0 ]) {
		color("red") { cube([ width, 300, 30 ]); }
	}

	thiccness = 40;
	radius = 100;
	// door
	color("orange") {
		// TODO: fix the offset. it is not really working at the moment bc of
		// the cylinders
		door_step_offset = 150;

		difference() {
			translate([ 0, 0, door_step_offset ]) {
				hull() {
					rotate([ 90, 0, 0 ]) {
						// CAUTION: this part is a little confusing:
						// becayse everything is rotated by 90 degrees on the
						// x-axis, the coordinates have to be the following, in
						// order to be absolute [x, y, z] -> [x, z, y]

						// bottom left
						translate([ 0 + radius, 0, 0 ]) {
							cylinder(r = radius, h = thiccness);
						}

						// top left
						translate([ 0 + radius, height, 0 ]) {
							cylinder(r = radius, h = thiccness);
						}

						// bottom right
						translate([ width - radius, 0, 0 ]) {
							cylinder(r = radius, h = thiccness);
						}

						translate([ width - radius, height, 0 ]) {
							cylinder(r = radius, h = thiccness);
						}
					}
				}
			}

			// windows
			translate([ 100, orientation == 1 ? -2 : 1, 900 ]) {
				window(600, 1200, thiccness + 4, radius / 2);
			}

			// windows
			translate([ 800, orientation == 1 ? -1 : 1, 900 ]) {
				window(600, 1200, thiccness + 4, radius / 2);
			}
		}
	}

	// windows
	translate([ 100, thiccness / 2 * (orientation == 1 ? 1 : -1), 900 ]) {
		window(600, 1200, thiccness + 4, radius / 2);
	}

	// windows
	translate([ 800, thiccness / 2 * (orientation == 1 ? 1 : -1), 900 ]) {
		window(600, 1200, thiccness + 4, radius / 2);
	}
}

module window(width, heigth, depth, corner_radius, rotation = 0) {
	hull() {
		rotate([ 90 + rotation, 0, 0 ]) {
			// CAUTION: this part is a little confusing:
			// becayse everything is rotated by 90 degrees on the x-axis,
			// the coordinates have to be the following, in order to be absolute
			// [x, y, z] -> [x, z, y],

			// bottom left
			translate([ corner_radius, 0, 0 ]) {
				cylinder(h = depth, r = corner_radius);
			}

			// top left
			translate([ corner_radius, heigth, 0 ]) {
				cylinder(h = depth, r = corner_radius);
			}

			// bottom right
			translate([ width - corner_radius, 0, 0 ,]) {
				cylinder(h = depth, r = corner_radius);
			}

			// top right
			translate([ width - corner_radius, heigth, 0 ]) {
				cylinder(h = depth, r = corner_radius);
			}
		}
	}
}

module connection() {
		color("black") cube([1000, 2820, 2500]);
}

module otherCarriage(width, length, height) {
	defaultCarriage(2820,26226,2000);
	depth = 20;
	windows = [
		[200, 0, 2000, 100,  2],
		[1500, 0, 1500, 100,  0],
		[7500, 0, 1000, 100,  1],
		[ 9500, 0, 1000, 100,  0],
		[ 12500, 0, 1000, 100,  0],
		[12800, 0, 2100, 100,  3],
		[15500, 0, 1000, 100,  1],
		[20500, 0, 1500, 100,  0],
		[23500, 0, 1500, 100,  0],

		// other side

		[200, 2870, 2000, 100,  2],
		[1500, 2870, 1500, 100,  0],
		[7500, 2870, 1000, 100,  1],
		[ 9500, 2870, 1000, 100,  0],
		[ 12500, 2870, 1000, 100, 0],
		[ 12800, 2870, 2100, 100, 3],
		[15500, 2870, 1000, 100,  1],
		[20500, 2870, 1500, 100,  0],
		[23500, 2870, 1500, 100,  0],
		];

		for (window_data = windows) {
			position = [for (i = [0:2]) window_data[i]];
			translate(position) {
				type = window_data[4];
				if (type == 0) {
					window(2500, 800, depth, 100);
				} else if (type == 1) {
					window(1500, 800, depth, 100);
				} else if (type == 2) {
					window(700, 700, depth, 100);
				} else if (type == 3) {
					window(2000, 200, depth, 100);
				}
			}}

		// doors left
		translate([ 5000, -5, door_elevation ]) { door(door_heigth, door_width, 1); }

		translate([ length - 8000 , -5, door_elevation ]) {
			door(door_heigth, door_width, 1);
		}

		// doors right
		translate([ 5000, 2821 + 40, door_elevation ]) {
			door(door_heigth, door_width, 0);
		}

		translate([ length - 8000, 2821 + 40, door_elevation ]) {
			door(door_heigth, door_width, 0);
		}
}

module cockpit() {
	difference() {
	color("Snow") {
		hull() {
			translate([0,0,0]) rotate([0,180,0]) {
				difference() {
					rotate([90, 0, 0]) rotate_extrude(angle = 360, convexity = 10) {
						translate([2500 , 0, 0]) circle(r = 1410);
					}

					union() {
						translate([-2500 - 1410, -1410, 0]) cube([(2500 + 1410) * 2, 1410 * 2, 2500 + 1410]);
						translate([-4000, -1410, -4000]) cube([4000, 1410 * 2, (2500 + 1410) * 2]);
					}}
			}
			translate([0, -1410, 0]) cube([1, 1410*2, 950]);
		}}
			 // translate([-1500,-1000,1500]) rotate([0,-80,0]) cube([1000,2000,2000]);
}
	 translate([-3790,-530,750]) rotate([0,10,0]) scale([0.6,0.6,0.6]) headlight();
	 translate([-3790, 530, 750]) rotate([0,10,0]) scale([0.6,0.6,0.6]) headlight();
};


module light(colour) {
	color(colour) linear_extrude(height=100, center=false, convexity=10, twist=0)
	circle(r=100); 
}

module headlight() {
	rotate([-90,0,-90]) { scale([1,2.5,0.2]) color("DimGrey") linear_extrude(height=500, center=false, convexity=10, twist=0)
	circle(r=250);
	translate([0, -450, -10]) light("Gold");
	translate([0, 450, -10]) light("black");
	translate([0, 0, -10]) light("black");
	}
}

module outerCarriage() {
translate([0, -300,250]) scale([1 * 0.7,1,1]) cockpit();
translate([0, -1700,0]) otherCarriage(2820,26226,2000);
}

module frontDoorStopper() {
	cylinder(r=175, h=300);
	cylinder(r=200, h=50);
}

module wholeTrain() {
outerCarriage();
translate([26230, -1700, 250]) connection();
translate([27230, -1700, 0]) carriage(2820, 26226, 2000);
translate([53456, -1700, 250]) connection();
translate([54452, -1700, 0]) carriage(2820, 26226, 2000);
translate([80678, -1700, 250]) connection();
mirror([1,0,0]) translate([-107900, 0, 0]) outerCarriage();
translate([5750, -1500, -320]) frontDoorStopper();
}

translate([-400, -35000, 0]) {
	 rotate([0,0,90]) wholeTrain();
	}
translate([21800, -15000, 0]) rotate([0,0,90]) wholeTrain();
