import smtplib
import ssl
import json

config = {}
with open("config.json") as f:
    config = json.load(f)
    print("[ * ] loaded config file.")
    print(config)

port = int(config["port"])
password = config["password"]
username = config["user"]

server_addr = config["server"]

context = ssl.create_default_context()

with smtplib.SMTP_SSL(server_addr, port, context=context) as server:
    try:
        server.login(username, password)
    except Exception as e:
        print(f"[ - ] error logging in: {e}")
        exit()

    mail = ""
    with open("mail.txt") as f:
        mail = f.read()

    server.sendmail("gott@3nt3.de", "niels-schlegel@gmx.de", mail)
