module rail_30000mm() {
	module rail(length) {

		difference() {

			cube([ 124, length, 154 ]);

			// top edge right
			translate([ 34, length / 2, 149 ])
				cube([ 12, length + 2, 12 ], center = true);

			translate([ 0, length / 2, 134 ])
				cube([ 60, length + 2, 70 ], center = true);

			// top edge left
			translate([ 86, length / 2, 149 ])
				cube([ 12, length + 2, 12 ], center = true);

			translate([ 120, length / 2, 134 ])
				cube([ 60, length + 2, 70 ], center = true);

			// middle part right
			translate([ 20, length / 2, 110 ]) rotate([ 0, 20, 0 ])
				cube([ 60, length + 2, 30 ], center = true);

			translate([ 8, length / 2, 64.5 ])
				cube([ 90, length + 2, 98 ], center = true);

			translate([ 10, length / 2, 25 ]) rotate([ 0, -7.5, 0 ])
				cube([ 80, length + 2, 30 ], center = true);

			// middle part left
			translate([ 100, length / 2, 110 ]) rotate([ 0, -20, 0 ])
				cube([ 60, length + 2, 30 ], center = true);

			translate([ 112, length / 2, 64.5 ])
				cube([ 90, length + 2, 98 ], center = true);

			translate([ 110, length / 2, 25 ]) rotate([ 0, 7.5, 0 ])
				cube([ 80, length + 2, 30 ], center = true);
		}

		// upper edge right
		translate([ 39, length, 144 ]) rotate([ 90, 0, 0 ])
			cylinder(h = length, r = 10);
		// upper edge left
		translate([ 81, length, 144 ]) rotate([ 90, 0, 0 ])
			cylinder(h = length, r = 10);
	}

	module schwelle() {

		module nut() {
			cylinder(20, 30, 30, $fn = 6);
			cylinder(35, 20, 20);
		}

		module clamp_right() {

			difference() {

				rotate([ 0, 5, 0 ]) cube([ 150, 200, 10 ]);

				translate([ -5, -100, -5 ]) rotate([ 0, 5, 10 ])
					cube([ 200, 100, 30 ]);

				translate([ -5, 200, -5 ]) rotate([ 0, 5, -10 ])
					cube([ 200, 100, 30 ]);

				translate([ 100, 60, -1.5 ]) cylinder(10, 31, 40);

				translate([ 100, 140, -2 ]) cylinder(10, 31, 40);
			}

			translate([ 100, 60, -2 ]) nut();

			translate([ 100, 140, -2 ]) nut();
		}

		module clamp_left() {

			difference() {

				rotate([ 0, -5, 0 ]) cube([ 150, 200, 10 ]);

				translate([ -48, 165, -5 ]) rotate([ 0, 0, 10 ])
					cube([ 200, 100, 30 ]);

				translate([ -65, -60, -5 ]) rotate([ 0, 0, -10 ])
					cube([ 200, 100, 30 ]);

				translate([ 50, 60, 9 ]) cylinder(10, 31, 40);

				translate([ 50, 140, 9 ]) cylinder(10, 31, 40);
			}

			translate([ 50, 60, 9 ]) nut();

			translate([ 50, 140, 9 ]) nut();
		}

		translate([300, 0, 0]) cube([ 2000, 250, 150 ]);

		translate([ 604, 25, 163.5 ]) clamp_right();

		translate([ 2039.5, 25, 163.5 ]) clamp_right();

		translate([ 405, 25, 150 ]) clamp_left();

		translate([ 1840.5, 25, 150 ]) clamp_left();
	}

	module rail_3000mm() {

		translate([ 520, 0, 150 ]) rail(3000);

		translate([ 1955.5, 0, 150 ]) rail(3000);

		for (i = [0:1500:2400]) {
			translate([ 0, i, 0 ]) schwelle();}
	}

	for (i = [0:3000:30000]) {
		translate([ 0, i, 0 ]) rail_3000mm();}
}

//rail_30000mm();
