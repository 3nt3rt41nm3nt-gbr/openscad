module ladder() {
yellow = "Yellow";
dG = "DimGray";
scale_size = 1000;
notch_size = 1000;

// needed for attachment, u can't see it directly
module triangle() {
  hull() {
    cylinder(h = 3, r = 0.5);
    translate([ 15, 0, 0 ]) cylinder(h = 3, r = 0.5);
    translate([ 0, 15, 0 ]) cylinder(h = 3, r = 0.5);
  }
}

module bar() {
  scale([ scale_size, scale_size, scale_size ]) {
    linear_extrude(height = 105, center = false, twist = 0, $fn = 12)
        circle(r = 0.5);  // thiCCness
  }
}

// notch (bar)
module notch() {
	cube([ 200, notch_size, notch_size * 100  + 5002 ]);
}


//	attachment connector
module connector(y) {
  translate([ 2850, y - 2200, -200 ]) {
    color(yellow) {
      scale([ 215, 200, 100 ]) {
        translate([ -12, -5, 0 ]) cube([ 43, 10, 3 ]);
      }
    }
  }
}

// generate connector
for (i = [0:10000:100000]) {
  connector(-i);
}

module attachment() {
  color(dG) hull() {
    translate([ 700, 0, 100 ]) rotate([ 0, -90, 0 ]) scale([ 20, 20, 100 ])
        triangle();
    cube([ 1000, 2000, 100 ]);
  }
  translate([ 290, 1250, 10 ]) screw();
  translate([ 750, 1250, 10 ]) screw();
}
// one screw consists of
module partialScrew() {
  linear_extrude(height = 3, center = false, twist = 0, $fn = 12)
      circle(r = 1);
}

module screw() {
  rotate([ -7, 0, 0 ]) scale([ 50, 50, 5 ]) for (i = [0:3:20]) {
    randNum = round(rands(0, 8, 1)[0]);
    colors = [
		"Yellow", "DarkTurquoise", "SlateGray", "Chocolate", "DarkRed", "DarkMagenta", "DeepSkyBlue"
    ];
    color(colors[randNum]) {
      translate([ 0, 0, i + 35 ]) partialScrew();
    }
  }
}

rotate([ 90, 0, 0 ]) {
  color(yellow) {
    difference() {
      bar();
      translate([ -100, 0, -1 ]) notch();
    }
    translate([ 10000, 0, 0 ]) {
      difference() {
        bar();
        translate([ -100, 0, -1 ]) notch();
      }
    }
  }
}

for (i = [0:10000:100000]) {
  rotate([ 0, 0, -90 ]) {
    translate([ i + 1650, 470, 100 ]) attachment();
	}
}

for (i = [0:10000:100000]) {
  rotate([ 0, 0, 90 ]) translate([ i - 102600, -9550, 100 ]) attachment();
}
}

module placeLadder() {
  scale([0.06, 0.06, 0.06]) {
    translate([ -62500,1178720,-20000 ]) {
      rotate([90,180,0])
        ladder();
      }
  }
}

placeLadder();

